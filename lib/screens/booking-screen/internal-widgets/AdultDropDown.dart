import 'package:flutter/material.dart';

class AdultDropDown extends StatefulWidget {
  const AdultDropDown({
    Key? key,
  }) : super(key: key);

  @override
  State<AdultDropDown> createState() => _AdultDropDownState();
}

class _AdultDropDownState extends State<AdultDropDown> {
  String dropdownValue = "1";
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue.toString(),
      icon: const Icon(Icons.arrow_drop_down),
      elevation: 40,
      style: const TextStyle(color: Colors.black),
      underline: Container(
        height: 2,
        width: 10,
        //   color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? newValue) {
        setState(() {
          dropdownValue = newValue!;
        });
      },
      items: <String>['1', '2', '3', '4', "5", "6"]
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
