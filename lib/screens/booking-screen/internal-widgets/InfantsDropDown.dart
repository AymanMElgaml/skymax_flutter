import 'package:flutter/material.dart';

class InfantsDropDown extends StatefulWidget {
  const InfantsDropDown({
    Key? key,
  }) : super(key: key);

  @override
  State<InfantsDropDown> createState() => _InfantsDropDownState();
}

class _InfantsDropDownState extends State<InfantsDropDown> {
  String dropdownValue = "0";
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue.toString(),
      icon: const Icon(Icons.arrow_drop_down),
      elevation: 40,
      style: const TextStyle(color: Colors.black),
      underline: Container(
        height: 5,
        width: 15,
        //   color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? newValue) {
        setState(() {
          dropdownValue = newValue!;
        });
      },
      items: <String>["0", '1', '2', '3', '4', "5", "6"]
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
