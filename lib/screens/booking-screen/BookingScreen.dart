// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';

import 'internal-widgets/AdultDropDown.dart';
import 'internal-widgets/ChildrenDropDown.dart';
import 'internal-widgets/InfantsDropDown.dart';

class BookingScreen extends StatelessWidget {
  const BookingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: CustomAppBar(
        screenTitle: "Booking",
      ),
      body: SingleChildScrollView(
        child: Center(
          child: SizedBox(
            width: width * .9,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: width * .9,
                  child: AutoSizeText(
                    "Swimming with Dolphins & Snorkeling Tour & Lunch",
                    maxLines: 2,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff606060),
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          const Icon(
                            Icons.access_alarm,
                            color: Colors.orange,
                            size: 20,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          AutoSizeText(
                            'Duration: 9 hours',
                            style: TextStyle(
                              color: Color(0xff333333),
                              fontSize: 10,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Text("30 USD",
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Color(0xfff26749),
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ))
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: [
                    InputContainer(
                      widget: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Adults",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xff808080),
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          AdultDropDown(),
                        ],
                      ),
                    ),
                    InputContainer(
                      widget: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Children",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xff808080),
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          ChildrenDropDown(),
                        ],
                      ),
                    ),
                    InputContainer(
                      widget: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Infants",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xff808080),
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          InfantsDropDown(),
                        ],
                      ),
                    ),
                    InputContainer(
                      widget: Text("TripDate"),
                    ),
                    InputContainer(
                      widget: const TextField(
                        decoration:
                            InputDecoration.collapsed(hintText: 'Copoun Code'),
                      ),
                    ),
                    InputContainer(
                      widget: const TextField(
                        decoration:
                            InputDecoration.collapsed(hintText: 'Room Number'),
                      ),
                    ),
                    Container(
                      width: width * .9,
                      height: height * .15,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border: Border.all(
                            color: const Color(0xffececec), width: 2),
                        color: const Color(0xffffffff),
                      ),
                      child: const TextField(
                        decoration:
                            InputDecoration.collapsed(hintText: 'Notes'),
                        maxLines: 5,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: width * .43,
                          height: height * .07,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: const Color(0xfff26749)),
                            onPressed: () {},
                            child: const Text(
                              'Close',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Montserrat',
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: width * .43,
                          height: height * .07,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: Colors.orange[300]),
                            onPressed: () {},
                            child: const Text(
                              'Book Now',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Montserrat',
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class InputContainer extends StatelessWidget {
  Widget widget;
  InputContainer({
    Key? key,
    required this.widget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width * .9,
      height: height * .07,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        border: Border.all(color: const Color(0xffececec), width: 2),
        color: const Color(0xffffffff),
      ),
      child: widget,
    );
  }
}
