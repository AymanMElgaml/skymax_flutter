// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/screens/activity-details-screen/internal-widgets/IncludedSection.dart';
import 'package:skymax_application_v1/screens/activity-details-screen/internal-widgets/NotIncludedSection.dart';
import 'package:skymax_application_v1/screens/activity-details-screen/internal-widgets/WhatToBringSection.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';

import 'internal-widgets/CarouselAndTitleSection.dart';
import 'internal-widgets/HighlightsSection.dart';
import 'internal-widgets/OverviewSection.dart';

class ActivityDetailsScreen extends StatelessWidget {
  const ActivityDetailsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: CustomAppBar(
        screenTitle: "Sea Trip",
      ),
      bottomNavigationBar: CustomBottomNavigationBar(),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              CarouselAndTitleSection(),
              HighlightsSection(),
              SizedBox(
                height: 10,
              ),
              OverviewSection(),
              IncludedSection(),
              NotIncludedSection(),
              WhatToBringSection(),
            ],
          ),
        ),
      ),
    );
  }
}
