// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class WhatToBringSection extends StatelessWidget {
  const WhatToBringSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Container(
      height: 200,
      width: width * .9,
      child: Stack(
        children: [
          Positioned(
            top: 10,
            child: Container(
              margin: EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
              child: Column(
                children: [
                  Container(
                    width: width * .9,
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Color(0xff1160a4).withOpacity(0.10000000149011612),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Wrap(
                      runSpacing: 5,
                      spacing: 10,
                      children: [
                        Text(
                          '⭐  Get to another beautiful reef  ',
                          style: TextStyle(
                            color: Color(0xff333333),
                            fontSize: 13,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          '⭐ Get to another beautiful reef  ',
                          style: TextStyle(
                            color: Color(0xff333333),
                            fontSize: 13,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          '⭐ Get to another beautiful reef  ',
                          style: TextStyle(
                            color: Color(0xff333333),
                            fontSize: 13,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          '⭐ Get to another beautiful reef  ',
                          style: TextStyle(
                            color: Color(0xff333333),
                            fontSize: 13,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          '⭐ Get to another beautiful reef  ',
                          style: TextStyle(
                            color: Color(0xff333333),
                            fontSize: 13,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 1,
            left: 30,
            // right: 0,
            // alignment: Alignment(0, 0.5),
            child: Container(
              width: width * .30,
              height: height * .04,
              decoration: BoxDecoration(
                color: Color(0xff333333),
                borderRadius: BorderRadius.circular(13),
              ),
              child: Center(
                child: Text(
                  "⭐ What To Bring",
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Color(0xffffffff),
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
