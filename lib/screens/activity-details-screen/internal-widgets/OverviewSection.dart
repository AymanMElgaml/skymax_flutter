import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class OverviewSection extends StatelessWidget {
  const OverviewSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(right: 20, left: 20, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Overview",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff333333),
              fontSize: 16,
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
              "Take advantage of the marvelous warm weather in Hurghada with a full day out on the Red Sea. Travel by boat out to Giftun Island, where you will spend a relaxing day snorkeling, exploring the island, and enjoying a buffet lunch on the boat.\n\nAfter you are picked up from your hotel, transfer to a boat docked on the Red Sea. Then, head out on the water, stopping off at a variety of spots to catch a glimpse of the colorful underwater life. In addition to seeing fish and coral, you will also have the chance to swim with dolphins.",
              style: TextStyle(
                fontFamily: 'Montserrat',
                color: Color(0xff333333),
                fontSize: 13,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
              ))
        ],
      ),
    );
  }
}
