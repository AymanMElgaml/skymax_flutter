import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class CarouselAndTitleSection extends StatelessWidget {
  const CarouselAndTitleSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.all(25),
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Container(
            width: 10,
            height: 10,
            color: Colors.blue[200],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Row(
                  children: [
                    const Icon(
                      Icons.access_alarm,
                      color: Colors.orange,
                      size: 20,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    AutoSizeText(
                      'Duration: 9 hours',
                      style: TextStyle(
                        color: Color(0xff333333),
                        fontSize: 10,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    const Icon(
                      Icons.add_location_outlined,
                      color: const Color(0xff00aa71),
                      size: 20,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    AutoSizeText(
                      'View in map',
                      style: TextStyle(
                        color: Color(0xff333333),
                        fontSize: 10,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            width: width * .9,
            child: AutoSizeText(
              "Swimming with Dolphins & Snorkeling Tour & Lunch",
              maxLines: 2,
              style: TextStyle(
                fontFamily: 'Montserrat',
                color: Color(0xff606060),
                fontSize: 17,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "30 USD",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xfff26749),
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
