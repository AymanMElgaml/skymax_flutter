import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/screens/home-screen/HomeScreen.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';
import 'package:skymax_application_v1/shared_widgets/InputContainer.dart';

import 'internal-widgets/GenderRadioButtons.dart';
import 'internal-widgets/LanguagesDropDown.dart';
import 'internal-widgets/NationalitiesDropDown.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        screenTitle: "Registration",
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            width: width * .8,
            margin: EdgeInsets.all(10),
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: const [
                    Text(
                      "Booking NO:",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xff606060),
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      "7484816",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xff808080),
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Wrap(
                  runSpacing: 20,
                  spacing: 20,
                  children: [
                    InputContainer(
                      widget: const TextField(
                        decoration: InputDecoration.collapsed(hintText: 'Name'),
                      ),
                    ),
                    InputContainer(
                      widget: const TextField(
                        decoration:
                            InputDecoration.collapsed(hintText: 'E-Mail'),
                      ),
                    ),
                    InputContainer(
                      widget: const TextField(
                        decoration:
                            InputDecoration.collapsed(hintText: 'Hotel'),
                      ),
                    ),
                    InputContainer(
                      widget: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "Language",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xff808080),
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          LanguagesDropDown(),
                        ],
                      ),
                    ),
                    InputContainer(
                      widget: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "Nationality",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xff808080),
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          NationalitiesDropDown(),
                        ],
                      ),
                    ),
                    InputContainer(
                      widget: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Birthday"),
                          TextButton(
                            child: Text('Choose Date'),
                            onPressed: () {
                              DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime(2018, 3, 5),
                                  maxTime: DateTime(2013, 6, 7),
                                  onChanged: (date) {
                                print('change $date');
                              }, onConfirm: (date) {
                                print('confirm $date');
                              },
                                  currentTime: DateTime.now(),
                                  locale: LocaleType.en);
                            },
                          ),
                        ],
                      ),
                    ),
                    InputContainer(
                      widget: GenderRadioButtons(),
                    ),
                    InputContainer(
                      widget: const TextField(
                        decoration:
                            InputDecoration.collapsed(hintText: 'Phone Number'),
                      ),
                    ),
                    Container(
                      width: width * .77,
                      height: height * .07,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: const Color(0xfff26749)),
                        onPressed: () => Get.to(HomeScreen()),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Register',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Montserrat',
                                fontSize: 16,
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Icon(
                              Icons.arrow_forward_outlined,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
