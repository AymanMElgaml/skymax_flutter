import 'package:flutter/material.dart';

class LanguagesDropDown extends StatefulWidget {
  const LanguagesDropDown({
    Key? key,
  }) : super(key: key);

  @override
  State<LanguagesDropDown> createState() => _LanguagesDropDownState();
}

class _LanguagesDropDownState extends State<LanguagesDropDown> {
  String dropdownValue = 'English';
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      icon: const Icon(Icons.arrow_drop_down),
      elevation: 40,
      style: const TextStyle(color: Colors.black),
      // underline: Container(
      //   height: 2,
      //   color: Colors.deepPurpleAccent,
      // ),
      onChanged: (String? newValue) {
        setState(() {
          dropdownValue = newValue!;
        });
      },
      items: <String>['English', 'French', 'Arabic', 'German']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
