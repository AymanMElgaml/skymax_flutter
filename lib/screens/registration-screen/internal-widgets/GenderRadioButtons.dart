import 'package:flutter/material.dart';

class GenderRadioButtons extends StatefulWidget {
  const GenderRadioButtons({
    Key? key,
  }) : super(key: key);

  @override
  State<GenderRadioButtons> createState() => _GenderRadioButtonsState();
}

enum GenderEnum { male, female }

class _GenderRadioButtonsState extends State<GenderRadioButtons> {
  GenderEnum? _gender = GenderEnum.male;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            width: 140,
            height: 100,
            child: ListTile(
              title: const Text('Male'),
              leading: Radio<GenderEnum>(
                value: GenderEnum.male,
                groupValue: _gender,
                onChanged: (GenderEnum? value) {
                  setState(() {
                    _gender = value;
                  });
                },
              ),
            ),
          ),
          Container(
            width: 155,
            height: 100,
            child: ListTile(
              title: const Text('Female'),
              leading: Radio<GenderEnum>(
                value: GenderEnum.female,
                groupValue: _gender,
                onChanged: (GenderEnum? value) {
                  setState(() {
                    _gender = value;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
