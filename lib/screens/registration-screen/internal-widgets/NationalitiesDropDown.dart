import 'package:flutter/material.dart';

class NationalitiesDropDown extends StatefulWidget {
  const NationalitiesDropDown({
    Key? key,
  }) : super(key: key);

  @override
  State<NationalitiesDropDown> createState() => _NationalitiesDropDownState();
}

class _NationalitiesDropDownState extends State<NationalitiesDropDown> {
  String dropdownValue = 'Egypt';
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      icon: const Icon(Icons.arrow_drop_down),
      elevation: 16,
      style: const TextStyle(color: Colors.black),
      onChanged: (String? newValue) {
        setState(() {
          dropdownValue = newValue!;
        });
      },
      items: <String>['Egypt', 'USA', 'Germany', 'KSA']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
