// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'package:skymax_application_v1/constants/dummy-data.dart';
import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/screens/activities-list-screen/ActivitiesListScreen.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';

class ActivityCategoriesScreen extends StatelessWidget {
  const ActivityCategoriesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: CustomAppBar(
        screenTitle: "Activities",
      ),
      body: SingleChildScrollView(
        child: Container(
          width: width,
          height: height,
          margin: EdgeInsets.only(top: 20),
          child: GridView.builder(
            // when you use it with single scroll page
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),

            //    scrollDirection: Axis.horizontal,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              //maxCrossAxisExtent: 200,
              crossAxisCount: 2,
              //  childAspectRatio: 1,
              crossAxisSpacing: 1,
              mainAxisSpacing: 1,
            ),
            itemCount: ActivityCategoriesList.length,
            itemBuilder: (context, index) {
              return Column(
                children: [
                  GestureDetector(
                    onTap: () => Get.to(ActivitiesListScreen()),
                    child: CategoriesCard(
                      title: ActivityCategoriesList[index].categoryTitle,
                      tripCounter: 10.toString(),
                      imageURL: ActivityCategoriesList[index].mainPictureURL,
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

class CategoriesCard extends StatelessWidget {
  String title;
  String tripCounter;
  String imageURL;
  CategoriesCard({
    Key? key,
    required this.title,
    required this.tripCounter,
    required this.imageURL,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            height: height * .18,
            width: width * .44,
            margin: EdgeInsets.only(
              left: 15,
              right: 15,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(30.0),
              child: Image.network(imageURL),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  SvgPicture.asset("Assets/SVG/Union 7.svg",
                      width: 35, semanticsLabel: 'Acme Logo'),
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff333333),
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "$tripCounter Tours",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff333333),
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
