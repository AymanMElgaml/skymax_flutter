import 'package:flutter/material.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';

import 'internal-widgets/BookingsCounterSection.dart';
import 'internal-widgets/BookingsSection.dart';

class MyBookingScreen extends StatelessWidget {
  const MyBookingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(screenTitle: "My Bookings"),
      bottomNavigationBar: CustomBottomNavigationBar(),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              BookingsCounterSection(),
              SizedBox(
                height: 20,
              ),
              BookingsSection(
                SectionTitle: "Ongoing Bookings",
              ),
              BookingsSection(
                SectionTitle: "History Bookings",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
