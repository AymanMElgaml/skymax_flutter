// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';
import 'package:dotted_line/dotted_line.dart';

class BookingsCounterSection extends StatelessWidget {
  const BookingsCounterSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;

    return Container(
      width: width * .92,
      height: height * .15,
      padding: const EdgeInsets.all(20),
      decoration: new BoxDecoration(
        color: const Color(0xfff3f3f3),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                "All Bookings",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff333333),
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 5),
                  child: DottedLine(
                    direction: Axis.horizontal,
                    lineLength: double.infinity,
                    dashColor: Color(0xffb4b4b4),
                  ),
                ),
              ),
              const Text(
                "24",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff333333),
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                "Ongoing Bookings",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff333333),
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 5),
                  child: DottedLine(
                    direction: Axis.horizontal,
                    lineLength: double.infinity,
                    dashColor: Color(0xffb4b4b4),
                  ),
                ),
              ),
              const Text(
                "01",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff333333),
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                "Previous Bookings",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff333333),
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 5),
                  child: DottedLine(
                    direction: Axis.horizontal,
                    lineLength: double.infinity,
                    dashColor: Color(0xffb4b4b4),
                  ),
                ),
              ),
              const Text(
                "22",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff333333),
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
