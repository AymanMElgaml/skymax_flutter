// ignore_for_file: non_constant_identifier_names, prefer_const_constructors, unnecessary_new, prefer_const_literals_to_create_immutables

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class BookingsSection extends StatelessWidget {
  String SectionTitle;
  BookingsSection({Key? key, required this.SectionTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Container(
      child: Column(children: [
        Text(
          SectionTitle,
          style: const TextStyle(
            fontFamily: 'Montserrat',
            color: const Color(0xff1160a4),
            fontSize: 16,
            fontWeight: FontWeight.w600,
            fontStyle: FontStyle.normal,
          ),
        ),
        Container(
          width: width * .92,
          height: height * .3,
          margin: const EdgeInsets.all(25),
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.blue,
            image: new DecorationImage(
              image: NetworkImage(
                  "https://www.arabserviceinterlaken.ch/photos/excursions/harder-kulm-interlaken-sommer-eiger-moench-jungfrau-02 (1)_ac17a_lg.jpg"),
              fit: BoxFit.cover,
            ),
            borderRadius: const BorderRadius.all(
              Radius.circular(10),
            ),
          ),
          child: Container(
            //color: Colors.grey.withOpacity(0.1),
            child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 6,
                    child: AutoSizeText(
                      "Swimming with Dolphins & Snorkeling Tour & Lunch",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xffffffff),
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      "75 USD",
                      style: const TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xfff89e54),
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 3,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.person,
                            color: Color(0xffffffff),
                            size: 15,
                          ),
                          SizedBox(width: 3),
                          new Text(
                            "2 Adults - 2 Children",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xffffffff),
                              fontSize: 10,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.remove_from_queue,
                            color: Color(0xffffffff),
                            size: 15,
                          ),
                          SizedBox(width: 3),
                          new Text(
                            "Ref: 80072382",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xffffffff),
                              fontSize: 10,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.access_alarm,
                              color: Color(0xffffffff),
                              size: 15,
                            ),
                            SizedBox(width: 3),
                            new Text(
                              "18:00 GMT",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xffffffff),
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.calendar_today,
                              color: Color(0xffffffff),
                              size: 15,
                            ),
                            SizedBox(width: 3),
                            new Text(
                              "26/7/2018",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xffffffff),
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.access_alarm,
                              color: Color(0xffffffff),
                              size: 15,
                            ),
                            SizedBox(width: 3),
                            new Text(
                              "3 hours",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xffffffff),
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                    ],
                  ),
                ],
              )
            ]),
          ),
        ),
      ]),
    );
  }
}
