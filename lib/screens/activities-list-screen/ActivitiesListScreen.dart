import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/dummy-data.dart';
import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';

import 'internal-widgets/ActivityCard.dart';

class ActivitiesListScreen extends StatelessWidget {
  const ActivitiesListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: CustomAppBar(
        screenTitle: "Sea Trip",
      ),
      bottomNavigationBar: CustomBottomNavigationBar(),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            // important
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                child: ListView.builder(
                  //these as well
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: activitiesList.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        ActivityCard(
                            mainPictureURL:
                                activitiesList[index].mainPictureURL,
                            activityTitle: activitiesList[index].activityTitle,
                            startingPrice: activitiesList[index].startingPrice,
                            duration: activitiesList[index].duration),
                        SizedBox(
                          height: 15,
                        )
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
