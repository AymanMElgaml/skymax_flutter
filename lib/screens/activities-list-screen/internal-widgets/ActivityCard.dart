// ignore_for_file: prefer_const_constructors

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/screens/activity-details-screen/ActivityDetailsScreen.dart';

class ActivityCard extends StatelessWidget {
  String mainPictureURL;
  String activityTitle;
  String startingPrice;
  String duration;
  ActivityCard({
    Key? key,
    required this.mainPictureURL,
    required this.activityTitle,
    required this.startingPrice,
    required this.duration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width * .92,
      height: height * .17,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
          color: Color(0xfff0f0f0),
          width: 5,
        ),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  topLeft: Radius.circular(10),
                ),
                child: Image.network(mainPictureURL),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: Column(
                children: [
                  Expanded(
                    flex: 2,
                    child: Container(
                      margin: EdgeInsets.all(5),
                      child: AutoSizeText(
                        activityTitle,
                        style: TextStyle(fontSize: 20),
                        maxLines: 2,
                      ),
                    ),
                  ),
                  const Divider(
                    color: Color(0xfff0f0f0),
                    thickness: 2,
                  ),
                  Expanded(
                    flex: 1,
                    child: Row(
                      children: [
                        SizedBox(
                          width: 12,
                        ),
                        const Icon(
                          Icons.access_alarm,
                          color: Colors.orange,
                          size: 20,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        AutoSizeText(
                          'Duration: $duration',
                          style: TextStyle(
                            color: Color(0xff333333),
                            fontSize: 10,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                      flex: 2,
                      child: Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              color: Color(0xfff89e54),
                              child: Center(
                                child: Text(
                                  "From: $startingPrice",
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    color: Color(0xffffffff),
                                    fontSize: 13,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () => Get.to(ActivityDetailsScreen()),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Color(0xff1160a4),
                                  borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                  ),
                                ),
                                child: Center(
                                  child: Text("Read More",
                                      style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        color: Color(0xffffffff),
                                        fontSize: 13,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                      )),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
