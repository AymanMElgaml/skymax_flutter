import 'package:flutter/material.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';

import 'internal-widgets/ButtonSection.dart';
import 'internal-widgets/FirstSection.dart';
import 'internal-widgets/UserDataSection.dart';

class UserProfileScreen extends StatelessWidget {
  const UserProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: CustomAppBar(
        screenTitle: "User Profile",
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              FirstSection(),
              SizedBox(
                height: 20,
              ),
              UserDataSection(),
              SizedBox(
                height: 20,
              ),
              ButtonSection(),
            ],
          ),
        ),
      ),
    );
  }
}
