// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class UserDataSection extends StatelessWidget {
  const UserDataSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return // Rectangle 119
        Container(
      height: height * .25,
      width: width * .92,
      padding: EdgeInsets.all(25),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        border: Border.all(color: const Color(0xffececec), width: 2),
        color: const Color(0xffffffff),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Icon(Icons.person_outline),
              SizedBox(
                width: 5,
              ),
              Text(
                "Personal Information's:",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff606060),
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 10,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                direction: Axis.vertical,
                children: [
                  Text(
                    "Full Name",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "Email",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "Phone",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "Date of birth",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "Nationality",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "Gender",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 10,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                direction: Axis.vertical,
                children: [
                  Text(
                    "Rafik Ramsis",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "rafikramsis@gmail.com",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "01065069929",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "01-01-1978",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "Egyptian",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  Text(
                    "Male",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xff808080),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
