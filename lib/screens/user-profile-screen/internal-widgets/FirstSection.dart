import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class FirstSection extends StatelessWidget {
  const FirstSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Container(
      width: width * .9,
      height: height * .15,
      child: Row(
        children: [
          Container(
            width: width * .25,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12.0),
              child: Image.network(
                  "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg",
                  fit: BoxFit.cover),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            width: width * .6,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                Text(
                  "Rafik Ramsis",
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Color(0xff808080),
                    fontSize: 17,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
                Text(
                  "rafikramsis@gmail.com",
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Color(0xff808080),
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
                Text(
                  "+2 010 12 34 5679",
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Color(0xff808080),
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: width * .6,
                  height: height * .04,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: const Color(0xff1160a4)),
                    onPressed: () {},
                    child: const Text(
                      'My Bookings',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
