// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/screens/activity-categories-screen/ActivityCategoriesScreen.dart';
import 'package:skymax_application_v1/screens/contact-screen/ContactsScreen.dart';
import 'package:skymax_application_v1/screens/feedback-screen/FeedbackScreen.dart';
import 'package:skymax_application_v1/screens/flight-info-screen/FlightInfoScreen.dart';
import 'package:skymax_application_v1/screens/my-booking-screen/MyBookingScreen.dart';
import 'package:skymax_application_v1/screens/settings-screen/SettingsScreen.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        screenTitle: "Home",
      ),
      bottomNavigationBar: CustomBottomNavigationBar(),
      body: GridView.count(
        primary: false,
        padding: const EdgeInsets.all(20),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        children: <Widget>[
          GestureDetector(
            //  onTap: ,
            child: HomeGridCell(
              title: "Guide Info",
              SVGpath: "Assets/SVG/Union 9.svg",
            ),
          ),
          GestureDetector(
            onTap: () => Get.to(ActivityCategoriesScreen()),
            child: HomeGridCell(
              title: "Activities",
              SVGpath: "Assets/SVG/Union 7.svg",
            ),
          ),
          GestureDetector(
            onTap: () => Get.to(FlightInfoScreen()),
            child: HomeGridCell(
              title: "Flight Info",
              SVGpath: "Assets/SVG/Group 325.svg",
            ),
          ),
          GestureDetector(
            onTap: () => Get.to(MyBookingScreen()),
            child: HomeGridCell(
              title: "My Bookings",
              SVGpath: "Assets/SVG/Union 8.svg",
            ),
          ),
          GestureDetector(
            onTap: () => Get.to(ContactsScreen()),
            child: HomeGridCell(
              title: "Contact Us",
              SVGpath: "Assets/SVG/Union 11.svg",
            ),
          ),
          GestureDetector(
            onTap: () => Get.to(FeedbackScreen()),
            child: HomeGridCell(
              title: "Feedback",
              SVGpath: "Assets/SVG/Union 10.svg",
            ),
          ),
          GestureDetector(
            //  onTap: () => Get.to(FeedbackScreen()),
            child: HomeGridCell(
              title: "Notifications",
              SVGpath: "Assets/SVG/message-square.svg",
            ),
          ),
          GestureDetector(
            onTap: () => Get.to(SettingsScreen()),
            child: HomeGridCell(
              title: "Settings",
              SVGpath: "Assets/SVG/Union 12.svg",
            ),
          ),
        ],
      ),
    );
  }
}

class HomeGridCell extends StatelessWidget {
  String title;
  String SVGpath;

  HomeGridCell({
    Key? key,
    required this.title,
    required this.SVGpath,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * .18,
      width: width * .45,
      margin: EdgeInsets.only(
        left: 13,
        right: 13,
      ),
      decoration: BoxDecoration(
        color: Color(0xfff3f3f3),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            SVGpath,
            semanticsLabel: 'Acme Logo',
            color: const Color(0xfff89e54),
          ),
          SizedBox(
            height: 10,
          ),
          Text(title,
              style: TextStyle(
                fontFamily: 'Montserrat',
                color: Color(0xff333333),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ))
        ],
      ),
    );
  }
}
