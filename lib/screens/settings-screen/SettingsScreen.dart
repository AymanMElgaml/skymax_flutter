// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:skymax_application_v1/screens/registration-screen/internal-widgets/LanguagesDropDown.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';
import 'package:skymax_application_v1/shared_widgets/InputContainer.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        screenTitle: "Settings",
      ),
      bottomNavigationBar: CustomBottomNavigationBar(),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              Text(
                "Choose Your language",
                style: const TextStyle(
                  fontFamily: 'Montserrat',
                  color: const Color(0xff1160a4),
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              InputContainer(
                widget: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Choose Your Language",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xff808080),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    LanguagesDropDown(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
