import 'package:flutter/material.dart';

class AgentsDropDown extends StatefulWidget {
  const AgentsDropDown({
    Key? key,
  }) : super(key: key);

  @override
  State<AgentsDropDown> createState() => _AgentsDropDownState();
}

class _AgentsDropDownState extends State<AgentsDropDown> {
  String dropdownValue = "Blue Sky";
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue.toString(),
      icon: const Icon(Icons.arrow_drop_down),
      elevation: 40,
      style: const TextStyle(color: Colors.black),
      underline: Container(
        height: 2,
        width: 10,
        //   color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? newValue) {
        setState(() {
          dropdownValue = newValue!;
        });
      },
      items: <String>['Blue Sky', 'Aque Tour', 'Blue Style']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
