import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/screens/registration-screen/RegistrationScreen.dart';

class ContinueAsGuest extends StatelessWidget {
  const ContinueAsGuest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width * .77,
      height: height * .35,
      child: Column(
        children: [
          const Divider(
            color: Colors.grey,
            // height: 10,
            thickness: 1,
          ),
          const SizedBox(
            height: 15,
          ),
          SvgPicture.asset("Assets/SVG/Group 6.svg",
              width: 50, semanticsLabel: 'Acme Logo'),
          SizedBox(
            height: 5,
          ),
          const Text(
            'Continue as guest',
            style: TextStyle(
              color: Color(0xff606060),
              fontSize: 18,
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Container(
            width: width * .77,
            height: height * .07,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: const Color(0xff1160a4)),
              onPressed: () => Get.to(RegistrationScreen()),
              child: const Text(
                'Continue',
                style: const TextStyle(
                  color: Colors.white,
                  fontFamily: 'Montserrat',
                  fontSize: 16,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
