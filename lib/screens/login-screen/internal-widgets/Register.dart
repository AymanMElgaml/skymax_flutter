import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/screens/registration-screen/RegistrationScreen.dart';
import 'package:skymax_application_v1/shared_widgets/InputContainer.dart';

import 'AgentsDropDown.dart';

class Register extends StatelessWidget {
  const Register({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width * .77,
      height: height * .35,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InputContainer(
            widget: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Select Operator",
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Color(0xff808080),
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
                AgentsDropDown(),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          InputContainer(
            widget: const TextField(
              decoration: InputDecoration.collapsed(hintText: 'Booking Number'),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Container(
            width: width * .77,
            height: height * .07,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: const Color(0xfff26749)),
              onPressed: () => Get.to(RegistrationScreen()),
              child: const Text(
                'Register',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Montserrat',
                  fontSize: 16,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
