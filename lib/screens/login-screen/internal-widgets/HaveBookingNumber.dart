// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class HaveBookingNumber extends StatelessWidget {
  HaveBookingNumber({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * .2,
      width: width * .8,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 10,
          ),
          SvgPicture.asset("Assets/SVG/Group 1.svg",
              width: 50, semanticsLabel: 'Acme Logo'),
          SizedBox(
            height: 10,
          ),
          Text(
            'Do you have\n a booking number',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0xff606060),
              fontSize: 20,
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
