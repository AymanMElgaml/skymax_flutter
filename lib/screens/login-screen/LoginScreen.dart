// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'internal-widgets/ContinueAsGuest.dart';
import 'internal-widgets/HaveBookingNumber.dart';
import 'internal-widgets/Register.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Guest Services",
          style: const TextStyle(
            fontFamily: 'Montserrat',
            color: Color(0xffffffff),
            fontSize: 18,
            fontWeight: FontWeight.w600,
            fontStyle: FontStyle.normal,
          ),
        ),

        // ignore: prefer_const_literals_to_create_immutables
        actions: [
          const Icon(
            Icons.phone_callback_outlined,
            color: Color(0xffffffff),
            size: 30,
          ),
          const SizedBox(width: 20),
        ],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              HaveBookingNumber(),
              Register(),
              ContinueAsGuest(),
            ],
          ),
        ),
      ),
    );
  }
}
