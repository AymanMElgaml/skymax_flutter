import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class SOSCntactsSection extends StatelessWidget {
  const SOSCntactsSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Container(
      width: width * .9,
      height: height * .30,
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        border: Border.all(color: const Color(0xffececec), width: 2),
        color: const Color(0xffffffff),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "SOS Contacts",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff606060),
              fontSize: 16,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          new Text(
            "In serious condition please call:",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff333333),
              fontSize: 12,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Risk Manager:",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff808080),
              fontSize: 13,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              new Text(
                "Ahmed Sherif",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff808080),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
              new Text(
                "+2 010 12 34 5679",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff808080),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Divider(
            color: Color(0xff333333),
            height: 4,
            thickness: 1,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Risk Manager:",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff808080),
              fontSize: 13,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              new Text(
                "Ahmed Sherif",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff808080),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
              new Text(
                "+2 010 12 34 5679",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff808080),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            color: Color(0xff333333),
            height: 4,
            thickness: 1,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Risk Manager:",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff808080),
              fontSize: 13,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              new Text(
                "Ahmed Sherif",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff808080),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
              new Text(
                "+2 010 12 34 5679",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff808080),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
