// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class SkyMaxContactSection extends StatelessWidget {
  const SkyMaxContactSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Container(
      width: width * .9,
      height: height * .30,
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        border: Border.all(color: const Color(0xffececec), width: 2),
        color: const Color(0xffffffff),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SvgPicture.asset("Assets/SVG/Group 439.svg",
                  semanticsLabel: 'Acme Logo'),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "Sky Max Contacts",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xff606060),
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "We looking forward to hear from you.",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xff333333),
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Address:",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff808080),
              fontSize: 13,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            "sheraton Road, Touristic Center Red Sea, Egypt",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff808080),
              fontSize: 13,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Divider(
            //   color: Color(0xff333333),
            color: Colors.grey[400],
            height: 4,
            thickness: 1,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            "Phone:",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff808080),
              fontSize: 13,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            "+20 65 340 42 16 17    -      +20 010 05 70 1500",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff808080),
              fontSize: 13,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Divider(
            color: Color(0xff333333),
            height: 4,
            thickness: 1,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            "Email:",
            style: TextStyle(
              fontFamily: 'Montserrat',
              color: Color(0xff808080),
              fontSize: 13,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "info@skymaxholidays.com",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff808080),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
