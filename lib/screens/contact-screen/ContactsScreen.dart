import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';

import 'internal-widgets/SOSCntactsSection.dart';
import 'internal-widgets/SkyMaxContactSection.dart';
import 'internal-widgets/TourLeaderSection.dart';

class ContactsScreen extends StatelessWidget {
  const ContactsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: CustomAppBar(
        screenTitle: "Contacts",
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              TourLeaderSection(),
              SizedBox(
                height: 20,
              ),
              SOSCntactsSection(),
              SizedBox(
                height: 20,
              ),
              SkyMaxContactSection(),
            ],
          ),
        ),
      ),
    );
  }
}
