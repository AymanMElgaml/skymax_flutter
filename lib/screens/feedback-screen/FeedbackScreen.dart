// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';
import 'package:skymax_application_v1/shared_widgets/InputContainer.dart';

import 'internal-widgets/ServiceDropDown.dart';
import 'internal-widgets/ShareFeedbackSection.dart';

class FeedbackScreen extends StatelessWidget {
  const FeedbackScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: CustomAppBar(
        screenTitle: "Feedback",
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: 15,
              ),
              ShareFeedbackSection(),
              InputContainer(
                widget: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Service",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xff808080),
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    ServiceDropDown(),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: width * .8,
                height: height * .2,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  border: Border.all(color: const Color(0xffececec), width: 2),
                  color: const Color(0xffffffff),
                ),
                child: const TextField(
                  decoration: InputDecoration.collapsed(hintText: 'Feedback'),
                  maxLines: 10,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: width * .8,
                height: height * .09,
                decoration: BoxDecoration(
                  color: Color(0xfff89e54),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Submit",
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Color(0xffffffff),
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Icon(
                        Icons.send,
                        color: Color(0xffffffff),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
