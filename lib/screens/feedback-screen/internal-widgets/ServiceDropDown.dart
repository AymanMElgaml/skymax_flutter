import 'package:flutter/material.dart';

class ServiceDropDown extends StatefulWidget {
  const ServiceDropDown({
    Key? key,
  }) : super(key: key);

  @override
  State<ServiceDropDown> createState() => _ServiceDropDownState();
}

class _ServiceDropDownState extends State<ServiceDropDown> {
  String dropdownValue = "Inquire";
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue.toString(),
      icon: const Icon(Icons.arrow_drop_down),
      elevation: 40,
      style: const TextStyle(color: Colors.black),
      underline: Container(
        height: 2,
        width: 10,
        //   color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? newValue) {
        setState(() {
          dropdownValue = newValue!;
        });
      },
      items: <String>['Inquire', 'Booking']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
