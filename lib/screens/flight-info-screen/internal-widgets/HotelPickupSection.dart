// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class HotelPickupSection extends StatelessWidget {
  const HotelPickupSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width * .92,
      height: height * .25,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        gradient: LinearGradient(
          begin: Alignment(0.5, 0),
          end: Alignment(1, 1),
          colors: [
            const Color(0xff646464),
            const Color(0xff333333),
          ],
        ),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 3,
                child: Wrap(
                  direction: Axis.vertical,
                  spacing: 10,
                  runSpacing: 10,
                  children: [
                    Text(
                      "Hotel Pickup",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xffffffff),
                        fontSize: 17,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    Text(
                      "You will picked up at",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xffffffff),
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    Text(
                      "17:00",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xffffffff),
                        fontSize: 17,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: SvgPicture.asset("Assets/SVG/Group 451.svg",
                    semanticsLabel: 'Acme Logo'),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            thickness: 2,
            height: 5,
            color: Color(0xff707070),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Transfer",
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Color(0xffffffff),
                        fontSize: 17,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      )),
                  Text(
                    "Need private transfer?",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xffffffff),
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ],
              ),
              Container(
                width: width * .29,
                height: height * .06,
                decoration: BoxDecoration(
                  color: Color(0xfff89e54),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.directions_car_filled_outlined,
                        color: Color(0xffffffff),
                      ),
                      SizedBox(width: 3),
                      Text(
                        "Request",
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Color(0xffffffff),
                          fontSize: 11,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
