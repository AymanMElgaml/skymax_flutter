import 'package:flutter/material.dart';

class BookingNumberSection extends StatelessWidget {
  const BookingNumberSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            children: const [
              Text(
                "Booking NO:",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff606060),
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                "7484816",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Color(0xff808080),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
