// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class FlightInfoSection extends StatelessWidget {
  const FlightInfoSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Container(
      width: width * .92,
      height: height * .25,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        gradient: LinearGradient(
          begin: Alignment(0.11851851642131805, 1),
          end: Alignment(1, 0),
          colors: [
            const Color(0xff1087ee),
            const Color(0xff1160a4),
          ],
        ),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: Container(
              child: Wrap(
                direction: Axis.vertical,
                spacing: 10,
                runSpacing: 10,
                children: [
                  const Text(
                    "Return Flight",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xffffffff),
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  const Text(
                    "30-10-2019",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xffffffff),
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  const Text(
                    "07:00 PM",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xffffffff),
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  const Text(
                    "07:00 PM",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xffffffff),
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  const Text(
                    "MS05 Hurghada – Berlin",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xffffffff),
                      fontSize: 13,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  const Text(
                    "Hurghada International Airport",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Color(0xffffffff),
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            //margin: EdgeInsets.only(left: 5, right: 5),
            child: DottedLine(
              lineThickness: 2,
              direction: Axis.vertical,
              lineLength: double.infinity,
              dashColor: Color(0xffb4b4b4),
            ),
          ),
          Expanded(
            flex: 1,
            child: SvgPicture.asset("Assets/SVG/Path 1285.svg",
                semanticsLabel: 'Acme Logo'),
          ),
        ],
      ),
    );
  }
}
