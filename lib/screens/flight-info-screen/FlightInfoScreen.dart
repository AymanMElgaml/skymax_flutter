// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:skymax_application_v1/shared_widgets/BottomNavigationBar.dart';
import 'package:skymax_application_v1/shared_widgets/CustomAppBar.dart';

import 'internal-widgets/BookingNumberSection.dart';
import 'internal-widgets/FlightInfoSection.dart';
import 'internal-widgets/HotelPickupSection.dart';

class FlightInfoScreen extends StatelessWidget {
  const FlightInfoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(),
      appBar: CustomAppBar(
        screenTitle: "Flight Screen",
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              BookingNumberSection(),
              FlightInfoSection(),
              SizedBox(
                height: 10,
              ),
              HotelPickupSection(),
            ],
          ),
        ),
      ),
    );
  }
}
