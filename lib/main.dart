// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skymax_application_v1/screens/login-screen/LoginScreen.dart';

import 'constants/themes.dart';
import 'screens/activities-list-screen/ActivitiesListScreen.dart';
import 'screens/activity-categories-screen/ActivityCategoriesScreen.dart';
import 'screens/activity-details-screen/ActivityDetailsScreen.dart';
import 'screens/booking-screen/BookingScreen.dart';
import 'screens/contact-screen/ContactsScreen.dart';
import 'screens/feedback-screen/FeedbackScreen.dart';
import 'screens/flight-info-screen/FlightInfoScreen.dart';
import 'screens/home-screen/HomeScreen.dart';
import 'screens/my-booking-screen/MyBookingScreen.dart';
import 'screens/registration-screen/RegistrationScreen.dart';
import 'screens/user-profile-screen/UserProfileScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      home: LoginScreen(),
      //  home: ActivityDetailsScreen(),
    );
  }
}
