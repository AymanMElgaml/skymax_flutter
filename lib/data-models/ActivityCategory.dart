class ActivityCategory {
  String categoryTitle;
  String mainPictureURL;
  ActivityCategory({
    required this.categoryTitle,
    required this.mainPictureURL,
  });
}
