class Activity {
  String activityTitle;
  String mainPictureURL;
  String duration;
  String startingPrice;
  Activity({
    required this.activityTitle,
    required this.mainPictureURL,
    required this.duration,
    required this.startingPrice,
  });
}
