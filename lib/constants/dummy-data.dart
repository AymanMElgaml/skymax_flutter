import 'package:skymax_application_v1/data-models/Activity.dart';
import 'package:skymax_application_v1/data-models/ActivityCategory.dart';

final List<ActivityCategory> ActivityCategoriesList = [
  ActivityCategory(
      categoryTitle: "Family Trip",
      mainPictureURL:
          "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg"),
  ActivityCategory(
      categoryTitle: "Sea Trip",
      mainPictureURL:
          "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg"),
  ActivityCategory(
      categoryTitle: "Adventure Trip",
      mainPictureURL:
          "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg"),
  ActivityCategory(
      categoryTitle: "Culture Trip",
      mainPictureURL:
          "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg"),
  ActivityCategory(
      categoryTitle: "Transfer",
      mainPictureURL:
          "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg"),
];

final List<Activity> activitiesList = [
  Activity(
      activityTitle: "Swimming with Dolphins & Snorkeling Tour & Lunch",
      duration: "9 hours",
      startingPrice: "30 USD",
      mainPictureURL:
          "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg"),
  Activity(
      activityTitle: "Sindbad Submarine Tour with Hotel Pickup",
      duration: "3 hours",
      startingPrice: "60 USD",
      mainPictureURL:
          "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg"),
  Activity(
      activityTitle: "Paradise Island Full–Day Snorkeling and Island Tour",
      duration: "10 hours",
      startingPrice: "70 USD",
      mainPictureURL:
          "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg"),
  Activity(
      activityTitle: "Sea Scope Submarine Trip",
      duration: "2 hours",
      startingPrice: "80 USD",
      mainPictureURL:
          "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg"),
  Activity(
      activityTitle: "Swimming with Dolphins & Snorkeling Tour & Lunch",
      duration: "9 hours",
      startingPrice: "90 USD",
      mainPictureURL:
          "https://www.arabserviceinterlaken.ch/photos/excursions/1.2r_fa87a_lg.jpg"),
];
