import 'package:flutter/material.dart';
import 'package:skymax_application_v1/constants/themes.dart';

class InputContainer extends StatelessWidget {
  Widget widget;
  InputContainer({
    Key? key,
    required this.widget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Container(
      width: width * .8,
      height: height * .08,
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Color(0xffffffff),
        // color: Colors.green,
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: Color(0xffececec),
          width: 2,
        ),
      ),
      child: widget,
    );
  }
}
