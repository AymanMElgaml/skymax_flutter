// ignore_for_file: must_be_immutable, prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skymax_application_v1/screens/user-profile-screen/UserProfileScreen.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  String screenTitle;

  CustomAppBar({
    Key? key,
    required this.screenTitle,
  })  : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(
        widget.screenTitle,
        style: const TextStyle(
          fontFamily: 'Montserrat',
          color: Color(0xffffffff),
          fontSize: 18,
          fontWeight: FontWeight.w600,
          fontStyle: FontStyle.normal,
        ),
      ),
      leading: Row(
        children: [
          SizedBox(
            width: 20,
          ),
          GestureDetector(
            onTap: () => Get.to(UserProfileScreen()),
            child: Icon(Icons.person),
          ),
        ],
      ),
      // ignore: prefer_const_literals_to_create_immutables
      actions: [
        const Icon(
          Icons.phone_callback_outlined,
          color: Color(0xffffffff),
          size: 30,
        ),
        const SizedBox(width: 20),
      ],
    );
  }
}
