// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  CustomBottomNavigationBar({Key? key}) : super(key: key);

  @override
  _CustomBottomNavigationBarState createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Theme(
        data: Theme.of(context).copyWith(
          // sets the background color of the `BottomNavigationBar`
          canvasColor: const Color(0xffffffff),

          // sets the active color of the `BottomNavigationBar` if `Brightness` is light
          // primaryColor: const Color(0xfff89e54),
          textTheme: Theme.of(context).textTheme.copyWith(
                caption: TextStyle(color: Colors.yellow),
              ),
        ), // sets the inactive color of the `BottomNavigationBar`
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: _currentIndex,
          // backgroundColor: const Color(0xfff89e54),
          selectedItemColor: const Color(0xfff89e54),
          //   unselectedItemColor: const Color(0xff808080),
          onTap: (value) {
            // Respond to item press.
            setState(() => _currentIndex = value);
          },
          items: [
            BottomNavigationBarItem(
              title: Text('Home'),
              icon: Icon(Icons.home),
            ),
            BottomNavigationBarItem(
              title: Text('Activities'),
              icon: Icon(Icons.star_border_rounded),
            ),
            BottomNavigationBarItem(
              title: Text('Tour Leader'),
              icon: Icon(Icons.person_pin_outlined),
            ),
            BottomNavigationBarItem(
              title: Text('Alert'),
              icon: Icon(Icons.mode_comment_outlined),
            ),
          ],
        ),
      ),
    );
  }
}
